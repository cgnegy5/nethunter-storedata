* Automatical Save&Restore of routing rules and iptables
* Preloaded results of the last scan + prescan on startup
* Text resize by gestures, tab switch by swipes, vibro reactions
* OS Fingerprinting system based on Satori format
* Port Scan upgraded to X-Scan with EternalBlue checker
* Scanning engine is greatly improved
* HSTS Spoofing with improved sslstrip
* Self-diagnosis for troubleshooting
* LOTS of other fixes and improvements
